"use strict";

const promqlTemplate = require("./promql-template");
const url = require("url");

function buildFurtherInvestigationAttachment(notification, context) {
  let links = [];

  let serviceDetails = context.serviceDetails;

  let runbookUrl = notification.commonAnnotations.runbook;
  if (runbookUrl) {
    links.push({
      text: `📚 ${notification.commonAnnotations.runbook_title || "Runbook"}`,
      type: "button",
      url: `https://gitlab.com/gitlab-com/runbooks/blob/master/${runbookUrl}`,
    });
  }

  for (let i = 1; i <= 5; i++) {
    let title = notification.commonAnnotations[`link${i}_title`];
    let url = notification.commonAnnotations[`link${i}_url`];

    if (title && url) {
      links.push({
        type: "button",
        text: title,
        url: url,
      });
    }
  }

  let gitlabDashboard = serviceDetails.getGitLabDashboard();
  if (gitlabDashboard) {
    links.push({
      text: `📊 GitLab Dashboard`,
      type: "button",
      url: gitlabDashboard,
    });
  }

  let grafanaFolder = serviceDetails.getGrafanaFolder();
  if (grafanaFolder) {
    links.push({
      text: "📈 More Dashboards",
      type: "button",
      url: `https://dashboards.gitlab.net/dashboards/f/${grafanaFolder}`,
    });
  }

  let prometheusLink = getPrometheusLink(notification);
  links.push({
    text: "📈 Prometheus",
    type: "button",
    url: prometheusLink,
  });

  let attachments = [];

  // Slack allows 5 buttons per attachment, so group them into groups of 5
  let count = 0;
  while (links.length) {
    count++;

    let attachment = {
      title: count === 1 ? "🕵️ Further Investigation..." : "",
      actions: [],
    };
    attachments.push(attachment);

    for (let i = 0; i < 5 && links.length; i++) {
      let link = links.shift();
      attachment.actions.push(link);
    }
  }

  let loggingLinks = serviceDetails.getLoggingLinks();
  if (loggingLinks && loggingLinks.length) {
    attachments.push({
      title: "🦌 Logging",
      text: loggingLinks.map((loggingLink) => `• <${loggingLink.permalink}|${loggingLink.name}>`).join("\n"),
    });
  }

  return attachments;
}

function getPrometheusLink(notification) {
  let count = 0;
  let query = {};
  for (let i = 1; i <= 5; i++) {
    let template = notification.commonAnnotations[`promql_template_${i}`];
    let rendered = promqlTemplate(template, notification);
    if (rendered) {
      query[`g${count}.range_input`] = "6h";
      query[`g${count}.expr`] = rendered;
      query[`g${count}.tab`] = "0";
      count++;
    }
  }

  return url.format({
    hostname: "thanos.gitlab.net",
    pathname: "/graph",
    protocol: "https",
    query: query,
  });
}

module.exports = buildFurtherInvestigationAttachment;

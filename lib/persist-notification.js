"use strict";

const elasticsearch = require("@elastic/elasticsearch");
const notificationUtils = require("./notification-utils");
const differenceInSeconds = require("date-fns/differenceInSeconds");
const logger = require("./logger");
const operationTimer = require("./operation-timer");
const client = process.env.ES_URL && process.env.ES_INDEX && new elasticsearch.Client({ node: process.env.ES_URL });

const ELASTICSEARCH_TIMEOUT = "10s";

function formatDate(date) {
  return date.valueOf();
}

async function findIncidentForGroupKey(startsAt, groupKey) {
  let formattedStartsAt = formatDate(startsAt);

  const response = await client.search({
    index: process.env.ES_INDEX,
    timeout: ELASTICSEARCH_TIMEOUT,
    body: {
      query: {
        bool: {
          filter: [
            {
              term: { groupKey: groupKey },
            },
            {
              term: { start: formattedStartsAt },
            },
          ],
        },
      },
    },
  });

  logger.info({
    message: "elasticsearch: search",
    response: response,
  });

  let body = response && response.body;
  if (!body) return [];

  if (!body.hits || !body.hits.hits || !body.hits.hits[0]) return [];

  let existing = body.hits.hits[0];
  if (!existing) return [];

  return [existing._id, existing._source];
}

async function createDoc(startsAt, groupKey, notification, slackMessageResponse) {
  let formattedStartsAt = formatDate(startsAt);

  let slack;
  if (slackMessageResponse) {
    slack = {
      ts: slackMessageResponse.ts,
      channel: slackMessageResponse.channel,
    };
  }

  let response = await client.index({
    index: process.env.ES_INDEX,
    timeout: ELASTICSEARCH_TIMEOUT,
    body: {
      start: formattedStartsAt,
      groupKey: groupKey,
      alert: notification,
      slack: slack,
    },
  });

  logger.info({
    message: "elasticsearch: insert",
    response: response,
  });

  return response;
}

// Persist the notification to elasticsearch, either by updating an existing document
// or opening a new doc
async function findNotification(notification) {
  // No persistence if it's not configured
  if (!client) {
    logger.warn({
      message: "skipping find as ES_URL and ES_INDEX env vars are not configured.",
    });
    return;
  }

  try {
    let startsAt = notificationUtils.earliestAlertStart(notification);
    let groupKey = notification.groupKey;

    if (!startsAt || !groupKey) {
      logger.warn({
        message: "skipping find as notification is missing a groupKey or start time",
      });

      return;
    }

    return await findIncidentForGroupKey(startsAt, groupKey);
  } catch (e) {
    logger.logException({ message: "unable to find notification" }, e);
  }
}

async function closeNotification(notification, id, source) {
  if (!id || !source) return;
  if (notification.status === "firing") return;

  let startsAt = notificationUtils.earliestAlertStart(notification);

  let closeTime = notificationUtils.lastestAlertEnd(notification) || new Date();
  let durationSeconds = differenceInSeconds(closeTime, startsAt);
  let formattedStartsAt = formatDate(startsAt);
  let formattedCloseTime = formatDate(closeTime);

  const response = await client.update({
    index: process.env.ES_INDEX,
    timeout: ELASTICSEARCH_TIMEOUT,
    id: id,
    body: {
      doc: Object.assign({}, source, {
        start: formattedStartsAt,
        end: formattedCloseTime,
        duration_s: durationSeconds,
        alert: notification,
      }),
    },
  });

  logger.info({
    message: "elasticsearch: close",
    response: response,
  });

  return response;
}

// Persist the notification to elasticsearch, either by updating an existing document
// or opening a new doc
async function persistNotification(notification, slackMessageResponse) {
  // No persistence if it's not configured
  if (!client) {
    logger.warn({
      message: "skipping persistence as ES_URL and ES_INDEX env vars are not configured.",
    });
    return;
  }

  try {
    let startsAt = notificationUtils.earliestAlertStart(notification);
    let groupKey = notification.groupKey;

    if (!startsAt || !groupKey) {
      logger.warn({
        message: "skipping persistence as notification is missing a groupKey or start time",
      });

      return;
    }

    let [id, existingDoc] = await findIncidentForGroupKey(startsAt, groupKey);

    if (existingDoc) {
      return [id, existingDoc];
    }

    return await createDoc(startsAt, groupKey, notification, slackMessageResponse);
  } catch (e) {
    logger.logException({ message: "unable to persist notification, ignoring" }, e);
  }
}

module.exports = {
  findNotification: operationTimer.wrapAsync("elastic-find-notification", findNotification),
  persistNotification: operationTimer.wrapAsync("elastic-persist-notification", persistNotification),
  closeNotification: operationTimer.wrapAsync("elastic-close-notification", closeNotification),
};

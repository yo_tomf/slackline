variables:
  FUNCTION_NAME: alertManagerBridge
  RUNTIME: nodejs${GL_ASDF_NODEJS_MAJOR_VERSION}
  REGION: us-central1
  TIMEOUT: 180s
  GCLOUD_SDK_DIR: /tmp

include:
  - local: .gitlab-ci-asdf-versions.yml

  # This template should be included in all Infrastructure projects.
  # It includes standard checks, gitlab-scanners, validations and release processes
  # common to all projects using this template library.
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/README.md#templatesstandardyml
  - project: 'gitlab-com/gl-infra/common-ci-tasks'
    ref: v1.70.1  # renovate:managed
    file: templates/standard.yml

# Version should match the runtime version being used on Google Cloud Functions.
image: node:${GL_ASDF_NODEJS_VERSION}

stages:
  - validate
  - test
  - deploy-test
  - integration-test
  - deploy-staging
  - deploy-prod
  - renovate_bot
  - release

verify:
  stage: validate
  needs: []
  before_script:
    - npm install
  script:
    - npm run verify
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

test:
  stage: test
  needs: []
  before_script:
    - npm install
  script:
    - npm test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

.install-gcloud:
  before_script:
    # Download and configure gcloud
    # Browse to /tmp so we download the SDK to it instead of the project dir
    - cd $GCLOUD_SDK_DIR

    # Download Google Cloud SDK
    - wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GL_ASDF_GCLOUD_VERSION}-linux-x86_64.tar.gz
    - tar zxf google-cloud-sdk-${GL_ASDF_GCLOUD_VERSION}-linux-x86_64.tar.gz && ./google-cloud-sdk/install.sh --quiet --usage-reporting=false --path-update=true
    - PATH="$GCLOUD_SDK_DIR/google-cloud-sdk/bin:${PATH}"

    # Update gcloud components and install
    - gcloud --quiet components update
    - gcloud components install beta

deploy-test:
  stage: deploy-test
  resource_group: test_deployment
  extends: .install-gcloud
  variables:
    # Override the function name to use the test function
    FUNCTION_NAME: alertManagerBridgeTest
  script:
    # Browse to the CI Project dir
    - cd $CI_PROJECT_DIR

    # Authenticate gcloud with a designated service account key
    - gcloud auth activate-service-account --key-file $STAGING_DEPLOYER_KEY

    # Deploy
    - gcloud -q
      --project $STAGING_PROJECT
      beta functions deploy $FUNCTION_NAME
      --region $REGION
      --env-vars-file $STAGING_ENV_YAML
      --runtime $RUNTIME
      --no-user-output-enabled
      --trigger-http
      --allow-unauthenticated
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

integration-test:
  stage: integration-test
  resource_group: test_deployment
  before_script:
    - npm install
  script:
    - npm run integration-test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

deploy-staging:
  stage: deploy-staging
  extends: .install-gcloud
  resource_group: staging_deployment
  script:
    # Browse to the CI Project dir
    - cd $CI_PROJECT_DIR

    # Authenticate gcloud with a designated service account key
    - gcloud auth activate-service-account --key-file $STAGING_DEPLOYER_KEY

    # Deploy
    - gcloud -q
      --project $STAGING_PROJECT
      beta functions deploy $FUNCTION_NAME
      --region $REGION
      --env-vars-file $STAGING_ENV_YAML
      --runtime $RUNTIME
      --no-user-output-enabled
      --trigger-http
      --allow-unauthenticated
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

deploy-prod:
  stage: deploy-prod
  extends: .install-gcloud
  resource_group: prod_deployment
  script:
    # Browse to the CI Project dir
    - cd $CI_PROJECT_DIR

    # Authenticate gcloud with a designated service account key
    - gcloud auth activate-service-account --key-file $PRODUCTION_DEPLOYER_KEY

    # Deploy
    - gcloud -q
      --project $PRODUCTION_PROJECT
      beta functions deploy $FUNCTION_NAME
      --region $REGION
      --env-vars-file $PRODUCTION_ENV_YAML
      --runtime $RUNTIME
      --no-user-output-enabled
      --trigger-http
      --allow-unauthenticated
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

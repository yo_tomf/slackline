/* eslint-disable no-undef */
var assert = require("assert");

describe("grafana-link-builder", function () {
  describe("#getGrafanaSnapshotUrlForAlert()", function () {
    it("should return a valid URL", function () {
      var grafanaLinkBuilder = require("../../lib/grafana-link-builder");
      let x = grafanaLinkBuilder.getGrafanaSnapshotUrlForAlert(require("../data/payload.json"));
      assert.equal(
        "https://dashboards.gitlab.net/render/d-solo/general-service/service-platform-metrics?from=1594886400000&to=1594972970098&panelId=9&tz=UTC&theme=dark&width=800&height=600&timeout=60&var-environment=gstg&var-type=web",
        x
      );
    });
  });
});
